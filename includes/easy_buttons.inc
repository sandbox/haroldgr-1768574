<?php
/**
 * @file
 * Admin form.
 */

/**
 * Implements form().
 */
function easy_buttons_config_form($form, &$form_state) {
  $peso = array(
    '-10', '-9', '-8', '-7', '-6', '-5', '-4', '-3', '-2', '-1', '0',
    '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
  );
  // Create Header.
  $header = array(
    t('ID'),
    t('Name'),
    t('Link'),
    t('Link type'),
    t('Image'),
    t('Img Style'),
    t('Weight'),
    '',
  );
  // Create table.
  $form['buttons'] = array(
    '#prefix' => '<div id="people">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => array(),
  );
  // Get the current buttons.
  $buttons = _get_current_easy_buttons();
  if ($buttons) {
    for ($i = 0; $i < count($buttons); $i++) {
      // Build the fields for this row in the table. We'll be adding
      // these to the form several times, so it's easier if they are
      // individual variables rather than in an array.

      // Button ID.
      $id = array(
        '#type' => 'markup',
        '#markup' => $buttons[$i]['id'],
      );
      // Button Name.
      $name = array(
        '#type' => 'textfield',
        '#default_value' => $buttons[$i]['name'],
        '#size'=> 20,
      );
      // Link.
      $link = array(
        '#type' => 'textfield',
        '#default_value' => $buttons[$i]['link'],
        '#size'=> 20,
      );
      // Link type.
      $link_type = array(
        '#type' => 'select',
        '#options' => array(
          '_blank' => t('Other Window'),
          '_self' => t('Same Window'),
        ),
        '#default_value' => $buttons[$i]['link_type'],
      );
      // Image.
      $fid = array(
        '#type' => 'managed_file',
        '#upload_location' => 'public://easy_buttons/',
        '#default_value' => $buttons[$i]['fid'],
        '#validators' => array(
          'file_validate_extensions' => array('png', 'jpg', 'jpeg'),
          'file_validate_size' => array(1048576),
        ),
      );
      // Image Style.
      $options = array('_default' => t('Default'));
      foreach (image_styles() as $key => $value) {
        $options[$key] = $key;
      }
      $img_style = array(
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => $buttons[$i]['img_style'],
      );
      // Weight.
      $weight = array(
        '#type' => 'select',
        '#options' => $peso,
        '#default_value' => $buttons[$i]['weight'],
      );
      // Delete button.
      $delete = array(
        '#type' => 'checkbox',
        '#title' => t('<b><span class="delete-item">Delete</span></b>'),
        '#default_value' => FALSE,
      );

      // Include the fields so they'll be rendered and named
      // correctly, but they'll be ignored here when rendering as
      // we're using #theme => table.
      //
      // Note that we're using references to the variables, not just
      // copying the values into the array.
      $form['buttons'][$buttons[$i]['id']] = array(
        'id' => &$id,
        'name' => &$name,
        'link' => &$link,
        'link_type' => &$link_type,
        'fid' => &$fid,
        'img_style' => &$img_style,
        'weight' => &$weight,
        'delete' => &$delete,
      );

      // Now add references to the fields to the rows that
      // `theme_table()` will use.
      //
      // As we've used references, the table will use the very same
      // field arrays as the FAPI used above.
      $form['buttons']['#rows'][$buttons[$i]['id']] = array(
        array('data' => &$id),
        array('data' => &$name),
        array('data' => &$link),
        array('data' => &$link_type),
        array('data' => &$fid),
        array('data' => &$img_style),
        array('data' => &$weight),
        array('data' => &$delete),
      );

      // Because we've used references we need to `unset()` our
      // variables. If we don't then every iteration of the loop will
      // just overwrite the variables we created the first time
      // through leaving us with a form with 3 copies of the same fields.
      unset($id);
      unset($name);
      unset($link);
      unset($link_type);
      unset($fid);
      unset($img_style);
      unset($weight);
      unset($delete);
    }
  }
  // Ends Table.

  // Add Button.
  $form["fieldset_element"] = array(
    '#type' => 'fieldset',
    '#title' => t('Add Button'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form["fieldset_element"]["easy_buttons_add"] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#attributes' => array(
      'placeholder' => t('Button name..'),
    ),
  );
  // Advance Options.
  $form["fieldset_adop"] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $options = array();
  foreach (image_styles() as $key => $value) {
    $options[$key] = $key;
  }
  $form["fieldset_adop"]['image_cache'] = array(
    '#title' => t('Default Image Style'),
    '#type' => 'select',
    '#default_value' => variable_get('easy_buttons_image_cache'),
    '#options' => $options,
    '#weight' => 1,
  );
  $form["fieldset_adop"]['fixed'] = array(
    '#title' => t('Enable Floating block.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('easy_buttons_fix_enable'),
    '#weight' => 2,
  );
  $form["fieldset_adop"]['view_mode'] = array(
    '#title' => t('Floating In:'),
    '#type' => 'select',
    '#default_value' => variable_get('easy_buttons_view_mode', 'home'),
    '#options' => array(
      'home' => t('Home'),
      'all' => t('All Site'),
    ),
    '#weight' => 3,
  );
  // Buttons.
  $form['submits']['submit_save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#name' => 'save',
    '#weight' => 10,
  );
  $form['submits']['submit_add'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#name' => 'add',
    '#weight' => 11,
  );
  return $form;
}

/**
 * Implements form_submit().
 */
function easy_buttons_config_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#name'] == 'add') {
    $title = $form_state['values']['easy_buttons_add'];
    $add = easy_buttons_insert_in_table($title);
    if ($add) {
      drupal_set_message(t('Insert @name button', array('@name' => $title)));
    }
    else {
      drupal_set_message(t('Error Inserting @name button', array('@name' => $title)), 'warning');
    }
  }
  elseif ($form_state['clicked_button']['#name'] == 'save') {
    $form_state['rebuild'] = TRUE;
    // Update Buttons.
    easy_buttons_update_in_table($form_state['values']['buttons']);
    // Items to Delete.
    easy_buttons_delete_in_table($form_state['values']['buttons']);
    // variables Set.
    variable_set('easy_buttons_image_cache', $form_state['values']['image_cache']);
    variable_set('easy_buttons_fix_enable', $form_state['values']['fixed']);
    variable_set('easy_buttons_view_mode', $form_state['values']['view_mode']);
    // Set Message.
    drupal_set_message(t('Save Table'));
  }
}

/**
 * Implements form_validate().
 */
function easy_buttons_config_form_validate($form, &$form_state) {
  if ($form_state['clicked_button']['#name'] == 'add') {
    if (!$form_state['values']['easy_buttons_add']) {
      form_set_error('easy_buttons_add', t('Add a Title button'));
    }
  }
}

/**
 * Function to Search in table.
 * Review all created buttons in database.
 */
function _get_current_easy_buttons() {
  $ban = FALSE;
  $query = db_select('easy_buttons_table', 'v')
          ->fields('v')
          ->orderBy('weight', 'ASC');
  $result = $query->execute();
  if ($result) {
    foreach ($result as $key => $row) {
      $ban = TRUE;
      $return[$key] = get_object_vars($row);
    }
    if ($ban) {
      return $return;
    }
  }
  return FALSE;
}

/**
 * Function to Insert Button.
 * The new button to insert in table.
 * @param $name
 *   Name for new Button.
 */
function easy_buttons_insert_in_table($name) {
  $id = db_insert('easy_buttons_table')
    ->fields(array(
      'name' => $name,
    ))
    ->execute();
  if (isset($id)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Function to Update Table.
 * Current buttons, check if all changes.
 * @param $rows
 *   All buttons.
 */
function easy_buttons_update_in_table($rows) {
  if ($rows != NULL) {
    foreach ($rows as $key => $row) {
      db_update('easy_buttons_table')
        ->fields(array(
          'name' => $row['name'],
          'link' => $row['link'],
          'link_type' => $row['link_type'],
          'fid' => isset($row['fid']) ? $row['fid'] : NULL,
          'img_style' => isset($row['img_style']) ? $row['img_style'] : variable_get('easy_buttons_image_cache'),
          'weight' => $row['weight'],
        ))
        ->condition('id', $key)
        ->execute();
    }
  }
}

/**
 * Function to Delete Button.
 * Current buttons, check if some button have a delete field active.
 * @param $rows
 *   All buttons.
 */
function easy_buttons_delete_in_table($rows) {
  if ($rows != NULL) {
    foreach ($rows as $key => $row) {
      if ($row['delete'] == TRUE) {
        db_delete('easy_buttons_table')
          ->condition('id', $key)
          ->execute();
        drupal_set_message(t('Delete Button "@name"', array('@name' => $row['name'])));
      }
    }
  }
}